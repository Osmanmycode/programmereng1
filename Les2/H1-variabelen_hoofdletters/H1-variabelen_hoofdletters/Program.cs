﻿using System;

namespace H1_variabelen_hoofdletters
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Typ een zin in: ");
            string invoer = Console.ReadLine();
            string uitvoer = invoer.ToUpper();
            Console.WriteLine("In hoofdletters: " + uitvoer);
            uitvoer = invoer.ToLower();
            Console.WriteLine("In kleine letters: " + uitvoer);
            uitvoer = invoer.Replace('a', 'x');
            Console.WriteLine("In kleine letters: " + uitvoer);
            Console.WriteLine("Je hebt " + invoer.Length + " karakters ingetypt!!!");
            // let op: nu vervangen we een char door een string
            // dan moeten beiden string zijn!!!!!!
            uitvoer = invoer.Replace("a", "PlopperDePlop");
            Console.ReadLine();
        }
    }
}
