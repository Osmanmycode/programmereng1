﻿using System;

namespace H4_Schrikkeljaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Schrikkeljaar");
            Console.Write("Typ een jaartal in: ");
            int jaartal = Convert.ToInt32(Console.ReadLine());
            string boodschap = "is geen schrikkeljaar.";
            if ((jaartal % 4) == 0)
            {
                boodschap = " is een schrikkeljaar.";
                if ((jaartal % 100) == 0)
                {
                    boodschap = " is geen schrikkeljaar";
                    if ((jaartal % 400) == 0)
                    {
                        boodschap = " is een schrikkeljaar";
                    }
                }
            }
            Console.WriteLine($"Het jaar {jaartal} {boodschap}");
            Console.ReadKey();
        }
    }
}
