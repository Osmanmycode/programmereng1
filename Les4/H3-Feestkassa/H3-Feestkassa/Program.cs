﻿using System;

namespace H3_Feestkassa
{
    class Program
    {
        static void Main(string[] args)
        {
            const int EenheidsprijsMosselenMetFriet = 20;
            const int EenheidsprijsKoninginnenhapjes = 10;
            const int EenheidsprijsIjsjes = 3;
            const int EenheidsprijsDranken = 2;
            Console.WriteLine("H3-Feestkassa");
            Console.Write("Mosselen met frietjes? ");
            string input = Console.ReadLine();
            int mosselenMetFriet = int.Parse(input);
            int totaal = mosselenMetFriet * EenheidsprijsMosselenMetFriet;
            Console.Write("Koninginnenhapjes? ");
            input = Console.ReadLine();
            int koninginnenhapjes = int.Parse(input);
            totaal += koninginnenhapjes * EenheidsprijsKoninginnenhapjes;
            Console.Write("Ijsjes? ");
            input = Console.ReadLine();
            int ijsjes = int.Parse(input);
            totaal += ijsjes * EenheidsprijsIjsjes;
            Console.Write("Dranken? ");
            input = Console.ReadLine();
            int dranken = int.Parse(input);
            totaal += dranken * EenheidsprijsDranken;
            Console.Write($"Het totaal te betalen bedrag is €{totaal}.");
        }
    }
}
