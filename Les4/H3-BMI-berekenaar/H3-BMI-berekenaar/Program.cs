﻿using System;

namespace H3_BMI_berekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("H3-BMI-berekenaar");
            Console.Write("Je gewicht: ");
            float gewicht = float.Parse(Console.ReadLine());
            Console.Write("Je lengte in meter: ");
            float lengte = float.Parse(Console.ReadLine());
            Console.WriteLine($"Je BMI is {(gewicht / Math.Pow(lengte, 2)):F2}");
            Console.ReadKey();
        }
    }
}
