﻿using System;

namespace ProSinaasappel
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("World wide Orange");
            /* Schrijf een programma die een kist met 16 sinaasappels vult
             * en dan in een container stopt. 
             * 
             */

            // buitenste lus dient voor de containers
            string container = "";
            for (int i = 1; i <= 4; i++)
            {
                // binnenste lus voor appelsienen in de doos
                string doos = "";
                for (int j = 1; j <= 16; j++)
                {
                    // doos = doos + "o";
                    doos += 'o';
                    // stop een appelsien in de doos
                }
                // container = container + doos;
                container += $"{doos}|";
                // stop de doos in de container
            }
            Console.WriteLine(container);
            Console.ReadLine();
        }
    }
}
