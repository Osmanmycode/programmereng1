﻿using System;

namespace ProArmstrongNummer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("PRO Armstrong nummer");
            Console.WriteLine("Typ en getal en ik zeg je als het een narcistisch getal is :)");
            string input = Console.ReadLine();

            // 1. Ik moet te weten komen hoeveel cijfers er in het getal zitten
            int aantalCijfersInHetGetal = input.Length;
            // 2. ik moet het getal splitsen in aparte cijfers
            // bv 350
            int getal = Convert.ToInt32(input);
            // als ik 350 deel door honderd krijg ik het eerste getal
            // als ik de rest deel door 10 krijg ik 5
            // als ik de rest deel door 1 krijg ik 0
            // hoe programmeer ik dat
            double rest = getal % 100;
            double cijfer = getal / 100;
            Console.WriteLine($"input: eerste cijfer is: {cijfer}, rest is {rest}");
            // hoe weet ik dat door honderd moet delen?
            // Dat hangt van het aantal cijfers in het getal af, indien 1 cijfer-> 1
            // indien 2 cijfers->10, indien 3 cijfers->100
            // hoe ik een link programmeren tussen 1 cijfer en 1?
            // tussen 2 cijfers en 10, tussen 3 cijfers en 100
            double deler = 1 * Math.Pow(cijfer, aantalCijfersInHetGetal);

            // oplossing
            int lengte = input.Length;
            rest = getal;
            for (int i = lengte; i > 0; i--)
            {
                deler = (1 * Math.Pow(10, i - 1));
                cijfer = rest / deler;
                rest = rest % deler;
            }
            Console.ReadLine();


        }
    }
}
