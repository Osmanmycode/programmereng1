﻿using System;

namespace ForVierkant
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("For vierkant");
            Console.Write("Aantal sterren in 1 zijde? ");
            int aantal = Convert.ToInt32(Console.ReadLine());
            /* De buitenste lus tekent het aantal lijnen
             * de binnenste het aantal sterren op elke lijn */
            for (int i = 1; i <= aantal; i++)
            {
                 // op één lijn vijf sterren
                for (int j = 1; j <= aantal; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine("buitenste lus om lijnen te printen ");
            }
            Console.ReadLine();
        }
    }
}
