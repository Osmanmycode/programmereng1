﻿using System;

namespace H2_weerstandberekenaar_deel1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Console.Write("Geef de waarde(uitgedrukt in een getal van 0 tot 9) van de eerste ring: ");
            string ring1 = Console.ReadLine();
            Console.Write("Geef de waarde(uitgedrukt in een getal van 0 tot 9) van de tweede ring: ");
            string ring2 = Console.ReadLine();
            Console.Write("Geef de waarde(uitgedrukt in een getal van - 2 tot 7) van de derde ring(exponent): ");
            string ring3 = Console.ReadLine();
            // nu gaan we berekenen, dus hebben we gehele getallen nodig
            int getal1 = Convert.ToInt32(ring1 + ring2);
            // Math.PI retourneert het pi getal, zo moet je het niet zelf intypen
            int getal2 = Convert.ToInt32(Math.Pow(10, Convert.ToInt32(ring3)));
            Console.WriteLine($"Resultaat is {getal1 * getal2} Ohm, ofwel {getal1}x{getal2}.");

            Console.WriteLine("╔═══════════════╦═══════════════╗");
            Console.WriteLine("║     ring 1    ║      ring2    ║");
            Console.WriteLine("╟───────────────╫───────────────╢");
            Console.WriteLine($"║     {ring1}         ║     {ring1}         ║");
            Console.WriteLine("╚═══════════════╩═══════════════╝");

            Console.WriteLine($"2 tot de 3 macht: {Math.Pow(2, 3)}");
            Console.WriteLine($"2 tot de 16 macht: {Math.Pow(2, 16)}");
            Console.WriteLine($"vierkantswortel uit 8: {Math.Sqrt(8)}");
            Console.WriteLine($"vierkantswortel uit 16: {Math.Sqrt(16)}");

            Console.WriteLine($"Het getal pi: {Math.PI}");
            Console.ReadKey();

        }
    }
}
