﻿using System;

namespace LerenWerkenMetTekstInCode
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            // om de console in UTF8 modus te zetten
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            string name = "Jan De Wilde";
            byte age = 22;
            string result = String.Format("Ik ben {0} en ik ben {1} jaar oud.", name, age);
            Console.WriteLine(result);
            result = $"Ik ben {name} en ik ben {age} jaar oud.";
            Console.WriteLine(result);
            float kostprijs = 123.50f;
            result = $"De kostprijs is {kostprijs:C}.";
            Console.WriteLine(result);

            result = string.Format(new System.Globalization.CultureInfo("nl-BE"), "In België staat € ervoor: {0:C}", kostprijs);
            Console.WriteLine(result);
            result = string.Format(new System.Globalization.CultureInfo("fr-FR"), "In Frankrijk staat € erna: {0:C}", kostprijs);
            Console.WriteLine(result);
            // System.Globalization.CultureInfo[];
            // cultures = System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures);
            result = $"Kostprijs is: {kostprijs:C}";
            Console.WriteLine(result);

            Console.ReadKey();
        }
    }
}
