﻿using System;

namespace H2_systeem_informatie
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            bool is64bit = Environment.Is64BitOperatingSystem;
            string pcName = Environment.MachineName;
            int procCount = Environment.ProcessorCount;
            string userName = Environment.UserName;
            long memory = Environment.WorkingSet; //zal ongeveer 11 MB teruggeven.
            string toonMij = $"Uw computer heeft een {(is64bit ? "64" : "32")} - bit besturingssysteem";
            Console.WriteLine(toonMij);
            toonMij = $"Uw computer heeft een 64 - bit besturingssysteem: {is64bit}";
            Console.WriteLine(toonMij);
            toonMij = $"De naam van uw pc is: {pcName}";
            Console.WriteLine(toonMij);
            toonMij = $"Uw pc heeft  {procCount} processorkernen.";
            Console.WriteLine(toonMij);
            toonMij = $"{userName} is uw gebruikersnaam.";
            Console.WriteLine(toonMij);
            toonMij = $"Je gebruikt {memory/Math.Pow(1024,2)} megabytes aan geheugen.";
            Console.WriteLine(toonMij);
            Console.ReadKey();
        }
    }
}
