﻿using System;

namespace WerkenMetString
{
    class Program
    {
        static void Main(string[] args)
        {
            string familieNaam = "Fadlalah";
            string voorNaam = "Osman";
            string stad = "Antwerpen";


            
            Console.WriteLine("Werken met string");

            Console.WriteLine($"Mijn naam is {voorNaam} {familieNaam} en ik woon in {stad}");

            Console.WriteLine($"Aantal Karakters in voornaam: {voorNaam.Length}");
            Console.WriteLine($"Aantal Karakters in familienaam: {familieNaam.Length}");


            Console.WriteLine($"Eerste karakter in familienaam: {familieNaam.Substring(0,1)}");
            Console.WriteLine($"Tweede karakter in je voornaam: {voorNaam.Substring(1,1)}");

            Console.WriteLine($"Derde en het vierde karakter in familienaam: {familieNaam.Substring(2,2)}");
            Console.WriteLine($"Het laaste karakter in familienaam: {familieNaam.Substring(7,1)}");

            Console.WriteLine($"Je initialen zijn: {voorNaam.Substring(0,1) + familieNaam.Substring(0,1)}");


            string volledigeNaam = "Osman Fadlalah";

            for (int i = 0; i <=volledigeNaam.Length-1; i++)
            {
                Console.WriteLine($"De letter {volledigeNaam.Substring(i, 1)}");
            }

            string reverse = String.Empty;
            char[] cArray = volledigeNaam.ToCharArray();
            for (int i = cArray.Length - 1; i > -1; i--)
            {
                reverse += cArray[i];
                
            }
            
            Console.WriteLine($"Mijn volledigenaam omgekeerd: {reverse}.");

            Console.ReadKey();
        }
    }
}
