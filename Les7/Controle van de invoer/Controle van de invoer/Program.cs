﻿using System;

namespace Controle_van_de_invoer
{
    class Program
    {
        static void Main(string[] args)
        {
           
            int getal = 0;
            string invoer;
            bool stop;
            int aantalgeldige = 0; 
            int aantalongeldige=0;

            int juistgetalsom = 0; 
            int foutgetalsom = 0;
            Console.WriteLine("Controle van de invoer");
            stop = true;
            do
            {
                Console.Write("Voer een getal in tussen 100.0 tot en met 120.0: ");
                getal = int.Parse(Console.ReadLine());

                if (getal < 100 | getal > 120)
                {

                    foutgetalsom = foutgetalsom + getal;
                    aantalongeldige++;
                }
                else
                {
                    juistgetalsom = juistgetalsom + getal;
                    aantalgeldige++;
                }

                Console.Write("Meer getallen invoeren? (j of n): ");
                invoer = Console.ReadLine().ToLower();

            } while (invoer == "j");

            Console.WriteLine($"Er zijn {aantalgeldige} geldig(e) en {aantalongeldige} ongeldig(e) getallen invoerd.");
            Console.WriteLine($"De totale som van geldige getallen is {juistgetalsom} en van de ongeldige getallen is {foutgetalsom}");
            Console.ReadKey();


        }
    }
}
