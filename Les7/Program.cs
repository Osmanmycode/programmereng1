﻿using System;
// using BlackMagic;
// using ModernWays;

namespace LerenWerkenMetMethoden
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leren werken met methoden");
            int keuze = 0;
            do
            {
                keuze = BlackMagic.Helpers.LeesGeheelGetalIn("1. Teken een vierkant\n2. Teken een pijl\n");
                switch (keuze)
                {
                    case 1:
                        char teken = BlackMagic.Helpers.LeesCharIn("Met welk teken? ");
                        BlackMagic.MeetkundigeVormen.TekenVierkant(teken);
                        break;
                    case 2:
                        BlackMagic.MeetkundigeVormen.TekenPijl();
                        break;
                    case 0:
                        Console.WriteLine("Programma beëindigd!!!");
                        break;
                    default:
                        Console.Beep();
                        Console.WriteLine("Kies tussen 1, 2 of 0 om te stoppen!!!!");
                        break;
                }
            } while (keuze != 0);


            //BlackMagic.MeetkundigeVormen.TekenVierkant();
            //BlackMagic.MeetkundigeVormen.TekenPijl();
            Console.ReadKey();
        }

    }
}
