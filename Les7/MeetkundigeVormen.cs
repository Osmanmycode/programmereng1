﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackMagic
{
    class MeetkundigeVormen
    {
        // parameter met een default (standaard) waarde
        public static void TekenVierkant(char teken = '*')
        {
            //Console.Write("Aantal sterren in 1 zijde? ");
            //int aantal = Convert.ToInt32(Console.ReadLine());
            int aantal = Helpers.LeesGeheelGetalIn("Aantal sterren in 1 zijde? ");
            /* De buitenste lus tekent het aantal lijnen
             * de binnenste het aantal sterren op elke lijn */
            for (int i = 1; i <= aantal; i++)
            {
                // op één lijn vijf sterren
                for (int j = 1; j <= aantal; j++)
                {
                    Console.Write(teken);
                }
                //Console.WriteLine("buitenste lus om lijnen te printen ");
                Console.WriteLine();
            }

        }

        // parameter met een default (standaard) waarde
        public static void TekenPijl(char teken = '*')
        {
            //Console.Write("Hoeveel sterren? ");
            //int aantal = Convert.ToInt32(Console.ReadLine());
            int aantal = Helpers.LeesGeheelGetalIn("Hoeveel sterren?");
            Console.WriteLine("For doordenker");
            for (int i = 1; i <= aantal; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(teken);
                }
                Console.WriteLine();
            }
            for (int i = aantal - 1; i > 0; i--)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
        }
    }
}
